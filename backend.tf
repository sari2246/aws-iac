# create s3 and create dynamo db
# create s3

resource "aws_s3_bucket" "mybucket" {
  bucket = "s3bucketforterraformstatefile0909"
}
resource "aws_s3_bucket_versioning" "versioning_ex" {
  bucket = aws_s3_bucket.mybucket.id
  versioning_configuration {
    status = "Enabled"
  }
  
}
resource "aws_s3_bucket_server_side_encryption_configuration" "encrypt_example" {
  bucket= aws_s3_bucket.mybucket.id
   rule {
    apply_server_side_encryption_by_default {
      sse_algorithm     = "AES256"
     }
    }
}
# create dynamo db
resource "aws_dynamodb_table" "state_lock" {
  name = "my-state-lock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}