terraform {
  backend "s3" {
    bucket = "s3bucketforterraformstatefile0909"
    dynamodb_table = "my-state-lock"
    key="global/mystatefile/terraform.tfstate" #statef ile stores in this path in s3 bucket
    region="us-east-1"
    encrypt=true 
  } 
}
provider "aws" {
  region = var.regionname
}