output "pb_sn" {
  value = aws_subnet.pub_subnet.id
}
output "sg" {
  value = aws_security_group.sg.id
}