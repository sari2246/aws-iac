#ec2 instance creation with the security group used in VPC module
resource "aws_instance" "server" {
  ami ="ami-00beae93a2d981137"
  instance_type = "t2.micro"
  subnet_id = var.sn
  security_groups = [var.sg]
    tags = {
        Name= "myserver"
    }
}